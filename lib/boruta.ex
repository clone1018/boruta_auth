defmodule Boruta do
  @moduledoc """
  Boruta is the core of an OAuth provider giving business logic of authentication and authorization.

  [Documentation](README.md)
  """
end
