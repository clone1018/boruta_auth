defmodule Boruta.Oauth.Authorization.ResourceOwner do
  @moduledoc """
  Resource owner authorization
  """

  import Boruta.Config, only: [resource_owners: 0]

  alias Boruta.Oauth.Error
  alias Boruta.Oauth.ResourceOwner

  @doc """
  Authorize the resource owner corresponding to the given params.

  ## Examples
      iex> authorize(id: "id")
      {:ok, %User{...}}
  """
  @spec authorize(
    [email: String.t(), password: String.t()] |
    [resource_owner: ResourceOwner.t()]
  ) ::
    {:error,
     %Error{
       :error => :invalid_resource_owner,
       :error_description => String.t(),
       :format => nil,
       :redirect_uri => nil,
       :status => :unauthorized
     }}
    | {:ok, user :: ResourceOwner.t()}
  def authorize(username: username, password: password) do
    with {:ok, resource_owner} <- resource_owners().get_by(username: username),
      :ok <- resource_owners().check_password(resource_owner, password) do
      {:ok, resource_owner}
    else
      {:error, reason} ->
        {:error, %Error{
          status: :unauthorized,
          error: :invalid_resource_owner,
          error_description: reason
        }}
    end
  end
  def authorize(resource_owner: %ResourceOwner{} = resource_owner) do
    {:ok, resource_owner}
  end
  def authorize(_) do
    {:error, %Error{
      status: :unauthorized,
      error: :invalid_resource_owner,
      error_description: "Resource owner is invalid.",
      format: :internal
    }}
  end
end
